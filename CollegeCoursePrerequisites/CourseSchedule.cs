﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeCoursePrerequisites
{
    class CourseSchedule
    {
        List<KeyValuePair<string, string>> _courses;
        const string _invalid = "Rejected: circular dependencies or invalid input";
        const string _error = "Rejected with error: ";
        public CourseSchedule(string[] courses)
        {
            try
            {
                // considering both : with space and without space separators to avoid typo
                string[] separator = new string[] { ": ", ":" };

                // Convert to list of keyvaluepair and remove empty keys    
                _courses = courses.Select(x => new KeyValuePair<string, string>(x.Split(separator, StringSplitOptions.None)[0], x.Split(separator, StringSplitOptions.None)[1]))
                                  .Where(y => string.IsNullOrWhiteSpace(y.Key) == false).ToList();
            }
            catch
            {
                // Initialize the course list with 0 number of elements so it will reject the output in case of invalid inputs  
                _courses = new List<KeyValuePair<string, string>>();
            }
        }
        public string GetOrderedCourseSchedule()
        {
            try
            {
                // There should be atleast one course with no dependency
                // Otherwise it is definitely a circular dependency 
                if (_courses.Count() == 0) return _invalid;

                // Take all courses with no dependency at the beginning of list
                List<KeyValuePair<string, string>> outputList = _courses.FindAll(x => x.Value == "");

                // Loop through non-dependent courses and append their dependencies recursively 
                for (int i = 0; i < outputList.Count(); i++)
                {
                    // Case insensitive check of course name just to avaoid any typo
                    outputList.AddRange(_courses.FindAll(x => x.Value.ToLower() == outputList[i].Key.ToLower()));
                }

                // Circular dependency in case count of output list is not equalt to origional list
                if (outputList.Count() != _courses.Count()) return _invalid;

                // Comma separated string of courses with prededed prerequisites 
                return string.Join(", ", outputList.Select(y => y.Key));
            }
            catch (Exception ex)
            {
                return _error + ex.Message;
            }
        }
    }
}
