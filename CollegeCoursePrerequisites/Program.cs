﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeCoursePrerequisites
{
    class Program
    {
        static void Main(string[] args)
        {
            // Valid input (for testing) 
            //string[] InputCourses = new string[] { 
            //    "Basics: ",
            //    ": ",
            //    "jQuery: Javascript",
            //    "Javascript: HTML",
            //    "Boostrap: CSS",
            //    "CSS: HTML",
            //    "HTML: Basics",
            //    "C#: C",
            //    "C: Basics",
            //    "Fundamentals: ",
            //    "Communications: "
            //};

            // Valid input (example 1)
            string[] InputCourses = new string[] { 
                "Introduction to Paper Airplanes: ",
                "Advanced Throwing Techniques: Introduction to Paper Airplanes",
                "History of Cubicle Siege Engines: Rubber Band Catapults 101",
                "Advanced Office Warfare: History of Cubicle Siege Engines",
                "Rubber Band Catapults 101: ",
                "Paper Jet Engines: Introduction to Paper Airplanes"
            };

            // Invalid input (example 2)
            //string[] InputCourses = new string[] { 
            //    "Intro to Arguing on the Internet: Godwin’s Law",
            //    "Understanding Circular Logic: Intro to Arguing on the Internet",
            //    "Godwin’s Law: Understanding Circular Logic"
            //};

            CourseSchedule schedule = new CourseSchedule(InputCourses);
            Console.WriteLine("Output  =  " + schedule.GetOrderedCourseSchedule());
            Console.ReadKey();  
        }
    }
}
